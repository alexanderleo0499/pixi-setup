import { Texture, Sprite, SCALE_MODES } from 'pixi.js'
import app from '../app'
import { onDragStart } from '../modules/drag';

class Bunny{
    bunny = null 

    constructor(x=0, y=0){
        this.create(x, y)
    }

    create(x, y){
        const texture = Texture.from('https://pixijs.com/assets/bunny.png');
        // Scale mode for pixelation
        texture.baseTexture.scaleMode = SCALE_MODES.NEAREST;
        // create our little bunny friend..
        this.bunny = new Sprite(texture);

        // enable the bunny to be interactive... this will allow it to respond to mouse and touch events
        this.bunny.eventMode = 'static';

        // this button mode will mean the hand cursor appears when you roll over the bunny with your mouse
        this.bunny.cursor = 'pointer';

        // center the bunny's anchor point
        this.bunny.anchor.set(0.5);

        // make it a bit bigger, so it's easier to grab
        this.bunny.scale.set(3);

        // setup events for mouse + touch using
        // the pointer events
        // this.bunny.on('pointerdown', this.dragStart, this.bunny);

        // move the sprite to its designated position
        this.bunny.x = x;
        this.bunny.y = y;

        // add it to the stage
        app.stage.addChild(this.bunny);
    }

    activateDrag(){
        this.bunny.on('pointerdown', onDragStart, this.bunny);
    }    
}

export default Bunny