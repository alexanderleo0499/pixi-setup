import {init} from './app'
import Bunny from './class/Bunny';
import { getCenterXandY } from './utils';

init()

const {x,y} = getCenterXandY()
const bunny = new Bunny(x,y)

bunny.activateDrag()

