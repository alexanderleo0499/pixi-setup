export const getCenterXandY = () => {
    const currentWidth = window.innerWidth
    const currentHeight = window.innerHeight

    return{
        x:currentWidth/2 ,
        y:currentHeight/2
    }
}