import { Application } from 'pixi.js'
import { COLOR_BG } from './constants/colors'
import { onDragEnd } from './modules/drag'

const app = new Application({
    background:COLOR_BG,
    resizeTo:window
})

export const init = () => {
    document.body.appendChild(app.view)
    //settings
    app.stage.eventMode = 'static'
    app.stage.hitArea = app.screen;
    app.stage.on('pointerup', onDragEnd);
    app.stage.on('pointerupoutside', onDragEnd);
}

export default app